import ipaddress

def ipv4toipv6(ipv4address):
    print("IPv4:", ipv4address)
    print("IPv6:", ipaddress.IPv6Address("2002::" + ipv4address).exploded)
    print("IPv6 Compressed:", ipaddress.IPv6Address("2002::" + ipv4address).compressed)
    print("\n")

ipv4toipv6("10.140.71.85")
ipv4toipv6("10.140.71.222")
ipv4toipv6("10.140.71.32")
ipv4toipv6("10.140.71.34")
ipv4toipv6("10.140.71.57")
ipv4toipv6("10.140.71.75")
ipv4toipv6("10.140.71.43")
ipv4toipv6("10.140.71.101")
ipv4toipv6("10.140.71.43")
ipv4toipv6("10.140.71.98")

