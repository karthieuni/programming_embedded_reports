#!/usr/bin/env python3
###########################
# ASS 23 - SERVER PROGRAM #
###########################
import socket								# Fetch the socket module
import pickle       						# Fetch the pickle module
import matplotlib.pyplot as plt				# Fetch the matplotlib.pyplot module
import matplotlib.animation as animation	# Fetch the matplotlib.animation module
import ssl     								# Fetch the ssl module

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)
DataCommingIn = True

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen()
print(' >>> Awaiting connection on IP: ',s.getsockname()[0],\
                          ' Port: ',s.getsockname()[1], " <<<")
connection, fromAddress = s.accept() # Wait and create connection object
print(' >>> Connection from:', fromAddress)

#Setup the graph
fig, yx = plt.subplots(figsize=(20,20))
yx.set(xlabel='Time Stamp (MM:SS:MS)', ylabel='Temperature (°)', title='Temperature Measurements')
yx.grid()

print(" >>> Data received, building graph, please stand by... ")

x = []
y = []

mySSLSocket = ssl.wrap_socket(connection,
                             server_side=True,
                             certfile="serverCertificate.crt",
                             keyfile="serverPrivate.key")

def doit(uh):
	receivedData = mySSLSocket.recv(100)

	temp = pickle.loads(receivedData)[0]
	timestamp = pickle.loads(receivedData)[1]

	print("Time(Min:Sec:Msec):", timestamp, "Temperature:", temp)

	y.append(temp)
	x.append(timestamp)

	xi = [i for i in range(0, len(x))]

	yx.clear()

	#Plotting int he graph
	yx.plot(xi, y, color ='b', linewidth=2)
	plt.xticks(xi, x, rotation=90)

	yx.margins(0.1)

while DataCommingIn: #Looks to see if input from user was numbers
	try:
		ani = animation.FuncAnimation(fig, doit, interval=2)
		
		#Set the fuctions of the graph
		fig.savefig("TemperatureMeasurements.png")
		plt.style.use('fivethirtyeight')

		#Show the graph with plots
		plt.show()
		
		if not receivedData:
			DataCommingIn = False
	except ValueError:	
		mySSLSocket.close()
		s.close()
		print(' >>> Connection closed')




